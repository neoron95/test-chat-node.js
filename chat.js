var express = require('express');
var app = express();

var mongoClient = require('mongodb').MongoClient;

var server = require('http').Server(app); // Подключаем http через app
var io = require('socket.io')(server,{'pingInterval': 30000, 'pingTimeout': 95000}); // Подключаем socket.io и указываем на сервер, а также временные интервалы, через которые будем отслеживать пользователей онлайн

var port = 3000;
var users=[]; // список пользователей онлайн

server.listen(port); // Теперь мы можем подключиться к нашему серверу через localhost:3000 при запущенном скрипте

app.use(express.static(__dirname + '/public')); // отправляем папку с клиентской частью пользователю

io.on('connection', function (socket) { // Создаем обработчик события

  var name = socket.request.connection.remoteAddress;
  if (name.substr(0,7) == "::ffff:") name=name.substr(7); // вырежем префикс IPV4 
  //name = socket.id; // для отладки на локальном сервере вместо ip использовать id (!!!)
  
  if (users.indexOf(name) == -1) { // если такой пользователь не был подключен, то добавляем его в список пользователей и отправляем сообщение остальным
	  var newLenght=users.push(name);
	  log({user: name, text: "New user"}); // логи
	  socket.broadcast.emit('newUser', name); // Отсылает событие 'newUser' всем подключенным, кроме текущего. На клиенте навешаем обработчик на 'newUser' (Отправляет клиентам событие о подключении нового юзера)
	  
	} else {
		log({user: name, text: "Reconnected"}); // логи
		var newLenght=users.push(name); // добавляем себя, т.к. будет удален предыдущий сеанс из онлайна
		socket.broadcast.emit('reUser', name); // сообщаем в чате, что пользователь переподключился
		}

  socket.broadcast.emit('onlineUsers', users);
  socket.emit('onlineUsers', users);
  socket.emit('userName', name); // Отправляем клиенту его имя
  
  socket.on('message', function(text, Name){  // сообщение в чат
	 log({user: name, text: "message", mess: text}); // логи
	 socket.broadcast.emit('message', Name + ": " + text +"\n");
	 socket.emit('message',"You: " + text + "\n");
	 
  });
  
  socket.on('disconnect', function(){
	var disUser = users.splice(users.indexOf(name),1); // удаляем отключившегося пользователя
	log({user: name, text: "Disconnected"}); // логи
	socket.broadcast.emit('disUser', disUser); // сообщение об отключении пользователя
	socket.broadcast.emit('onlineUsers', users);
	});


});


function log(text) { // открывает БД, помещает сообщение, закрывает БД
	mongoClient.connect("mongodb://localhost:27017/test", function(err, db){
	
	if (err) return console.log(err);
	
	var collect = db.collection("logs"); // имя коллекции
	
	collect.insertOne(text, function(err, result){
		if (err) return console.log(err);
		console.log(result.ops);
		db.close();
		}); 
	});
}

