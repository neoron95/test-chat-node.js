var port = 3000; // Указываем порт на котором у на стоит сокет
var socket = io.connect('http://localhost:' + port); // Объявляем сокет, с которым дальше будем работать и подключаемся к серверу через порт
var myName; // логин


$(document).on('click', 'button', function(){ // Прослушка кнопки на нажатие
	var message = $('input').val(); 
	socket.emit('message', message, myName); // Отправляем событие 'message' на сервер c текстом и логином
	$('input').val(null);
});

socket.on('userName', function(userName){ // Узнаем свой логин
	myName=userName;
	$('textarea').val($('textarea').val() + 'Your name is ' + myName + '\n');
});

socket.on('newUser', function(userName){ // Сообщение о новом пользователе
	$('textarea').val($('textarea').val() + 'New user has been connected ' + userName + '\n');
});


socket.on('reUser', function(userName){ // Сообщение о переподключении
	if (userName != myName) { // проверяем, не мы ли переподключились
		$('textarea').val($('textarea').val() + userName + ' is reconnected!\n'); // Это событие было отправлено всем кроме только что переподключенного
	  } else { // если мы, то разрываем соединение
		  $('textarea').val($('textarea').val()+'You are reconnected in other place');
		  socket.disconnect();
		  }
});

socket.on('disUser', function(userName){ // Сообщение об удалении
	if (userName != myName) { // проверяем, не мы ли переподключились
		$('textarea').val($('textarea').val() + userName + ' is disconnected!\n');
	  } // если мы, то ничего не пишем

});

socket.on('message', function(text){ // Новое сообщение в чат
	$('textarea').val($('textarea').val() + text);	
});

socket.on('onlineUsers', function(text){ // показывает список пользователей ONLINE
	$("#userList").text("Online: ");
	text.forEach(function(item, index,array){
		index+=1;
		$("#userList").text($("#userList").text() + index + ")" + item + " ");
	});
	
});


